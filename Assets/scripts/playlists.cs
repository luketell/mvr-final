﻿using System;
//using Newtonsoft.Json.Linq;

public class playlists
{
    public int? idplaylist { get; set; }
    public string playlist_name { get; set; }


    public playlists() { }
	public playlists(int IdPlaylist, string Playlist_Name)
	{	
		idplaylist = IdPlaylist;
		playlist_name = Playlist_Name;
    }

    public override string ToString()
    {
        return "IdPlaylist: " + idplaylist + "\nPlaylist_Name: " + playlist_name + '\n';

    }
    //Other properties, methods, events...

}

