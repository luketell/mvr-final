﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Storage and access of global variables used throughout the program
//Mostly options in the settings menu's that can be passed between scenes

public static class GlobalVariables
{
    private static double startCountTime;
    private static bool FPSflag = true;
    private static bool showName = true;
    private static bool showLatLong = true;
    private static bool showSOG = true;
    private static bool showROT = true;
    private static bool showHeading = true;
    private static List<dynamicAIS> globaldynamicShipsList;

    public static List<dynamicAIS> GlobaldynamicShipsList
    {
        get
        {
            return globaldynamicShipsList;
        }
        set
        {
            globaldynamicShipsList = value;
        }
    }

    public static double StartCountTime
    {
        get
        {
            return startCountTime;
        }
        set
        {
            startCountTime = value;
        }
    }

    public static bool FPSFlag
        {
            get
            {
                return FPSflag;
            }
            set
            {
                FPSflag = value;
            }
        }

    public static bool ShowName
    {
        get
        {
            return showName;
        }
        set
        {
            showName = value;
        }
    }

    public static bool ShowSOG
    {
        get
        {
            return showSOG;
        }
        set
        {
            showSOG = value;
        }
    }

    public static bool ShowROT
    {
        get
        {
            return showROT;
        }
        set
        {
            showROT = value;
        }
    }

    public static bool ShowHeading
    {
        get
        {
            return showHeading;
        }
        set
        {
            showHeading = value;
        }
    }

    public static bool ShowLatLong
    {
        get
        {
            return showLatLong;
        }
        set
        {
            showLatLong = value;
        }
    }
}


