﻿using System;
//using Newtonsoft.Json.Linq;

public class playlist_items
{
    public int idplaylist_item { get; set; }
    public int idplaylist { get; set; }
    public int idscenario { get; set; }
    public int? idnext { get; set; }
    public int? idprev { get; set; }
    public int index { get; set; }

    public playlist_items() { }
    public playlist_items(int Idplaylist_item, int IdPlaylist, int Idscenario,
    int Idnext, int Idprev, int Index)
    {
        idplaylist_item = Idplaylist_item;
        idplaylist = IdPlaylist;
        idscenario = Idscenario;
        idnext = Idnext;
        idprev = Idprev;
        index = Index;
    }

    public override string ToString()
    {
        return "\nIdplaylist_item: " + idplaylist_item + "IdPlaylist: " + idplaylist + "\nIdscenario " + idscenario +
                "\nIdnext " + idnext + "\nIdprev " + idprev + "\nIndex " + index + '\n';

    }
    //Other properties, methods, events...

}


