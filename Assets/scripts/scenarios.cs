﻿using System;
//using Newtonsoft.Json.Linq;

public class scenarios
{
    public int? idscenario { get; set; }
    public int? idsession { get; set; }
    public string time_start { get; set; }
    public string time_end { get; set; }
    public float? area { get; set; }
    public float? lat1 { get; set; }
    public float? long1 { get; set; }
    public float? lat2 { get; set; }
    public float? long2 { get; set; }
    public string name { get; set; }

    public scenarios() { }
	public scenarios(int Idscenario, int Idsession, string Time_start, string Time_end, float Area,
    float Lat1, float Long1, float Lat2, float Long2, string Name)
	{	
		idscenario = Idscenario;
		idsession = Idsession;
        time_start = Time_start;
        time_end = Time_end;
        area = Area;
        lat1 = lat1;
        long1 = Long1;
        lat2 = Lat2;
        long2 = Long2;
        name = Name;
    }

    public override string ToString()
    {
        return "\nIdscenario " + idscenario + "\nIdsession " + idsession + "\nTime_start " + time_start +
            "\nTime_end " + time_end + "\nArea " + area + "\nLat1 " + lat1 + "\nLong1 " + long1 +
            "\nLat2 " + lat2 + "\nLong2 " + long2 +  "\nName " + name   +'\n';
    }
    //Other properties, methods, events...

}

