﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

public class ConvertCoordinates : MonoBehaviour {
    public static float pi = 3.14159265358979F;
    void Start()
    {
        

        double latitude = 0;
        double longitude = 0;
        Console.WriteLine(latitude + " " + longitude);
        //Console.WriteLine(DegToRad(10F));
        ToLatLon(517554.79, 6974029.94, "56J", out latitude, out longitude);
        Console.WriteLine(latitude + " " + longitude);
        Console.WriteLine(ConvertToUtmString(-27.317612, 153.300652));
        //longitude":153.300652,"latitude":-27.317612

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void convertCaluclations()
    {  
            

    }
            //Console.ReadKey();
        

        /**
 * DegToRad
 *
 * Converts degrees to radians.
 * @param {number} deg degrees
 * @returns {number} radiens
 */


        public static float DegToRad(float deg)
        {
            return (deg / 180.0F * pi);
        }

        /**
         * RadToDeg
         *
         * Converts radians to degrees.
         * @param {number} rad radiens
         * @returns {number} degrees
         */
        public static float RadToDeg(float rad)
        {
            return (rad / pi * 180.0F);
        }

        public static double RadToDeg(double rad)
        {
            return (rad / pi * 180.0);
        }

        public static double angleFromCoordinate(float lat1, float long1, float lat2, float long2)
        {

            //var R = 6371e3; // metres
            var p1 = DegToRad(lat1);
            var p2 = DegToRad(lat2);
            var dp = DegToRad(lat2 - lat1);
            var dr = DegToRad(long2 - long1);

            var a = Math.Sin(dp / 2) * Math.Sin(dp / 2) +
                Math.Cos(p1) * Math.Cos(p2) *
                Math.Sin(dr / 2) * Math.Sin(dr / 2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            c = RadToDeg(c);
            c = Math.Min(c, 360 - c); // take the shortest path arround the earth
                                      //var d = R * c;
            return c;
        }



        public static double round40kmEarthApproxDistanceBetween2LatLongs(float lat1, float long1, float lat2, float long2)
        {
            var AngleBetweenLatLongs = angleFromCoordinate(lat1, long1, lat2, long2);
            return AngleBetweenLatLongs / 360 * 40000000; // under aproximation of earths circumference at 40,000,000 meters
        }



        public static void ToLatLon(double utmX, double utmY, string utmZone, out double latitude, out double longitude)
        {
            bool isNorthHemisphere = utmZone.Last() >= 'N';

            var diflat = -0.00066286966871111111111111111111111111;
            var diflon = -0.0003868060578;

            var zone = int.Parse(utmZone.Remove(utmZone.Length - 1));
            var c_sa = 6378137.000000;
            var c_sb = 6356752.314245;
            var e2 = Math.Pow((Math.Pow(c_sa, 2) - Math.Pow(c_sb, 2)), 0.5) / c_sb;
            var e2cuadrada = Math.Pow(e2, 2);
            var c = Math.Pow(c_sa, 2) / c_sb;
            var x = utmX - 500000;
            var y = isNorthHemisphere ? utmY : utmY - 10000000;

            var s = ((zone * 6.0) - 183.0);
            var lat = y / (c_sa * 0.9996);
            var v = (c / Math.Pow(1 + (e2cuadrada * Math.Pow(Math.Cos(lat), 2)), 0.5)) * 0.9996;
            var a = x / v;
            var a1 = Math.Sin(2 * lat);
            var a2 = a1 * Math.Pow((Math.Cos(lat)), 2);
            var j2 = lat + (a1 / 2.0);
            var j4 = ((3 * j2) + a2) / 4.0;
            var j6 = ((5 * j4) + Math.Pow(a2 * (Math.Cos(lat)), 2)) / 3.0;
            var alfa = (3.0 / 4.0) * e2cuadrada;
            var beta = (5.0 / 3.0) * Math.Pow(alfa, 2);
            var gama = (35.0 / 27.0) * Math.Pow(alfa, 3);
            var bm = 0.9996 * c * (lat - alfa * j2 + beta * j4 - gama * j6);
            var b = (y - bm) / v;
            var epsi = ((e2cuadrada * Math.Pow(a, 2)) / 2.0) * Math.Pow((Math.Cos(lat)), 2);
            var eps = a * (1 - (epsi / 3.0));
            var nab = (b * (1 - epsi)) + lat;
            var senoheps = (Math.Exp(eps) - Math.Exp(-eps)) / 2.0;
            var delt = Math.Atan(senoheps / (Math.Cos(nab)));
            var tao = Math.Atan(Math.Cos(delt) * Math.Tan(nab));

            longitude = ((delt * (180.0 / Math.PI)) + s) + diflon;
            latitude = ((lat + (1 + e2cuadrada * Math.Pow(Math.Cos(lat), 2) - (3.0 / 2.0) * e2cuadrada * Math.Sin(lat) * Math.Cos(lat) * (tao - lat)) * (tao - lat)) * (180.0 / Math.PI)) + diflat;
        }



        private static string GetBand(double latitude)
        {
            if (latitude <= 84 && latitude >= 72)
                return "X";
            else if (latitude < 72 && latitude >= 64)
                return "W";
            else if (latitude < 64 && latitude >= 56)
                return "V";
            else if (latitude < 56 && latitude >= 48)
                return "U";
            else if (latitude < 48 && latitude >= 40)
                return "T";
            else if (latitude < 40 && latitude >= 32)
                return "S";
            else if (latitude < 32 && latitude >= 24)
                return "R";
            else if (latitude < 24 && latitude >= 16)
                return "Q";
            else if (latitude < 16 && latitude >= 8)
                return "P";
            else if (latitude < 8 && latitude >= 0)
                return "N";
            else if (latitude < 0 && latitude >= -8)
                return "M";
            else if (latitude < -8 && latitude >= -16)
                return "L";
            else if (latitude < -16 && latitude >= -24)
                return "K";
            else if (latitude < -24 && latitude >= -32)
                return "J";
            else if (latitude < -32 && latitude >= -40)
                return "H";
            else if (latitude < -40 && latitude >= -48)
                return "G";
            else if (latitude < -48 && latitude >= -56)
                return "F";
            else if (latitude < -56 && latitude >= -64)
                return "E";
            else if (latitude < -64 && latitude >= -72)
                return "D";
            else if (latitude < -72 && latitude >= -80)
                return "C";
            else
                return null;
        }

        private static int GetZone(double latitude, double longitude)
        {
            // Norway
            if (latitude >= 56 && latitude < 64 && longitude >= 3 && longitude < 13)
                return 32;

            // Spitsbergen
            if (latitude >= 72 && latitude < 84)
            {
                if (longitude >= 0 && longitude < 9)
                    return 31;
                else if (longitude >= 9 && longitude < 21)
                    return 33;
                if (longitude >= 21 && longitude < 33)
                    return 35;
                if (longitude >= 33 && longitude < 42)
                    return 37;
            }

            return (int)Math.Ceiling((longitude + 180) / 6);
        }

        public static string ConvertToUtmString(double latitude, double longitude)
        {
            if (latitude < -80 || latitude > 84)
                return null;

            int zone = GetZone(latitude, longitude);
            string band = GetBand(latitude);

        //Transform to UTM
        //ProjNet.CoordinateSystems.Transformations.CoordinateTransformationFactory ctfac = new ProjNet.CoordinateSystems.Transformations.CoordinateTransformationFactory();
        //GeoAPI.CoordinateSystems.ICoordinateSystem wgs84geo = ProjNet.CoordinateSystems.GeographicCoordinateSystem.WGS84;
        //GeoAPI.CoordinateSystems.ICoordinateSystem utm = ProjNet.CoordinateSystems.ProjectedCoordinateSystem.WGS84_UTM(zone, latitude > 0);
        //GeoAPI.CoordinateSystems.Transformations.ICoordinateTransformation trans = ctfac.CreateFromCoordinateSystems(wgs84geo, utm);
        //double[] pUtm = trans.MathTransform.Transform(new double[] { longitude, latitude });

        //double easting = pUtm[0];
        //double northing = pUtm[1];
        double easting = 0;        
        double northing = 0;
        
        Debug.Log("" + northing + easting);
        return String.Format("{0}{1} {2:0} {3:0}", zone, band, easting, northing);
        }

    }


