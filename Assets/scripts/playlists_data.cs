﻿using System;
//using Newtonsoft.Json.Linq;

public class playlists_data
{
    public int? idplaylist { get; set; }
    public string playlist_name { get; set; }
    public int? idscenario { get; set; }
    public int? idsession { get; set; }
    public string scenario_name { get; set; }
    public float? area { get; set; }
    public string time_end { get; set; }
    public string time_start { get; set; }
    public int? idplaylist_item { get; set; }
    public int? index { get; set; }
    public int? idnext { get; set; }
    public int? idprev { get; set; }

    public playlists_data() { }
	public playlists_data(int IdPlaylist, string Playlist_Name, int Idscenario, int Idsession, string Scenario_Name,
    float Area, string Time_end, string Time_start, int Idplaylist_item, int Index, int Idnext, int Idprev)
	{	
		idplaylist = IdPlaylist;
		playlist_name = Playlist_Name;
		idscenario = Idscenario;
		idsession = Idsession;
		scenario_name = Scenario_Name;
		area = Area;
		time_end = Time_end;
		time_start = Time_start;
		idplaylist_item = Idplaylist_item;
		index = Index;
		idnext = Idnext;
		idprev = Idprev;
    }

    public override string ToString()
    {
        return "IdPlaylist: " + idplaylist + "\nPlaylist_Name: " + playlist_name + "\nIdscenario " + idscenario +
            "\nScenario_name " + scenario_name + "\nArea " + area + "\nTime_end " + time_end + "\nTime_start " + time_start +
            "\nIdplaylist_item " + idplaylist_item + "\nIndex " + index + "\nIdnext " + idnext + "\nIdprev " + idprev + '\n';

    }
    //Other properties, methods, events...

}

