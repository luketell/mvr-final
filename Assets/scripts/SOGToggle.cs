﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SOGToggle : MonoBehaviour {
    public Toggle toggle;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnValueChanged()
    {
        if (GlobalVariables.ShowSOG == true)
        {
            GlobalVariables.ShowSOG = false;
        }
        else
        {
            GlobalVariables.ShowSOG = true;
        }
    }
}
