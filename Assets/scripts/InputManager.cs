﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InputManager
{ 
	// Use this for initialization
	public static bool Fire1() {
        return Input.GetButtonDown("Fire1");
	}

    public static bool Fire2()
    {
        return Input.GetButtonDown("Fire2");
    }

	public static bool Fire3()
    {
        return Input.GetButtonDown("Fire3");
    }

    public static bool Fire4()
    {
        return Input.GetButtonDown("Fire4");
    }

    public static bool Fire5()
    {
        return Input.GetButtonDown("Fire5");
    }

    public static bool Jump()
    {
        return Input.GetButtonDown("Jump");
    }

}
