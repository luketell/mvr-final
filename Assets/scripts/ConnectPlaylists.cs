﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using UnityEngine.UI;
using System.Linq;
using System;

public class ConnectPlaylists : MonoBehaviour {
    public static List<playlists_data> playlistDataList = new List<playlists_data>();
    public static List<playlist_items> playlistItemsList = new List<playlist_items>();
    // Use this for initialization

    public Text text;
    public Text scenarioIDText;
    public Text sessionIDText;
    public Text playListNameText;
    public Dropdown playListSelect;
    public Text playListLabel;
    //public Text text;
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void checkValueSelected()
    {
        if (playListSelect.value == 0)
        {
            //Debug.Log("selected: 0 " + (playListLabel.text));
        }

        if (playListSelect.value == 1)
        {
            //Debug.Log("selected: 1 " + (playListLabel.text));
            foreach (var i in playlistDataList)
            {
                if (i.idplaylist == Convert.ToInt32(playListLabel.text))
                {
                    Debug.Log("This data is: " + i);
                    scenarioIDText.text = i.idscenario.ToString();
                    sessionIDText.text = i.idsession.ToString();
                    playListNameText.text = i.playlist_name.ToString();

                }
                                
            }
        }

        if (playListSelect.value == 2)
        {
            //Debug.Log("selected: 2 " + (playListLabel.text));
            foreach (var i in playlistDataList)
            {
                if (i.idplaylist == Convert.ToInt32(playListLabel.text))
                {
                    Debug.Log("This data is: " + i);
                    scenarioIDText.text = i.idscenario.ToString();
                    sessionIDText.text = i.idsession.ToString();
                    playListNameText.text = i.playlist_name.ToString();

                }
                
            }
        }

        if (playListSelect.value == 3)
        {
            //Debug.Log("selected: 3 " + (playListLabel.text));
            foreach (var i in playlistDataList)
            {
                if (i.idplaylist == Convert.ToInt32(playListLabel.text))
                {
                    Debug.Log("This data is: " + i);
                    scenarioIDText.text = i.idscenario.ToString();
                    sessionIDText.text = i.idsession.ToString();
                    playListNameText.text = i.playlist_name.ToString();

                }
                
            }
        }

        if (playListSelect.value == 4)
        {
            //Debug.Log("selected: 4 " + (playListLabel.text));
            foreach (var i in playlistDataList)
             {
                  if (i.idplaylist == Convert.ToInt32(playListLabel.text))
                  {
                       Debug.Log("This data is: " + i);
                       scenarioIDText.text = i.idscenario.ToString();
                       sessionIDText.text = i.idsession.ToString();
                       playListNameText.text = i.playlist_name.ToString();
                   }
                        
               }
         }

    }
                     
    public void downloadPlaylistData()
    {

        try
        {
            WebClient wc = new System.Net.WebClient();
            //List<dynamicAIS> list = new List<dynamicAIS>();
            var data = wc.DownloadString("http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/playlists_data.php?passwd=m3F4gaWX9M");

            //JArray dships = JArray.Parse(data);
            //var items = new JavaScriptSerializer().Deserialize<dynamicAIS>(data);
            
            var items = Newtonsoft.Json.JsonConvert.DeserializeObject<List<playlists_data>>(data);
            Debug.Log("CAN I SEE THIS!");
            foreach (var i in items)
            {
                if (!playlistDataList.Any(p => p.idplaylist == i.idplaylist))
                {
                    playlistDataList.Add(i);          
                }
                else
                {
                    //if there is a 
                }
                
            }
            string playlists = "";
            foreach (playlists_data item in playlistDataList)
            {
                Debug.Log(item);
                playlists += item + " \n";
                text.text = playlists;

            }            

        }
        catch (WebException e)
        {
            throw e;
            //Debug.Log("Invalid input sent to server");
        }
    }
}
