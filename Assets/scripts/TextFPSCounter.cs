﻿using UnityEngine;
using UnityEngine.UI;

// Display FPS on a Unity GUI Text Panel
// To use: Drag onto a game object with Text component
public class TextFPSCounter : MonoBehaviour
{
    public Text text;
    public bool show = false;

    private const int targetFPS =
#if UNITY_ANDROID // GEARVR
        80;
#else
        80;
#endif
    private const float updateInterval = 0.5f;

    private int framesCount;
    private float framesTime;
    private float fps = 80;

    void Start()
    {
        Application.targetFrameRate = 50;
        // no text object set? see if our gameobject has one to use
        if (text == null)
        {
            text = GetComponent<Text>();
        }
    }

    void Update()
    {
        if (GlobalVariables.FPSFlag == true)
        { 


        // monitoring frame counter and the total time
        framesCount++;
        framesTime += Time.unscaledDeltaTime;

        // measuring interval ended, so calculate FPS and display on Text
        if (framesTime > updateInterval)
        {
            if (text != null)
            {
                if (true)
                {
                    fps = framesCount / framesTime;
                    //Debug.Log("FPS: " + fps);
                    text.text = System.String.Format("{0:F1} FPS", fps);
                    text.color = (fps > (targetFPS - 5) ? Color.green :
                                 (fps > (targetFPS - 30) ? Color.yellow :
                                  Color.red));
                }
                else
                {
                    //text.text = "";
                }
            }
            // reset for the next interval to measure
            framesCount = 0;
            framesTime = 0;
        }
    }

    }
}