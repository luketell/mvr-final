﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenerateAISScript : MonoBehaviour {
    //http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/ais_voyages_dynamic.php?passwd=m3F4gaWX9M&session=314&limit=20
    public string baseAISString = "http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/ais_voyages_dynamic.php?passwd=";
    public string passwd = "m3F4gaWX9M";
    public string session = "";
    public string limit = "";
    public string mmsi = "";
    public string finalAISString = "";

    public Text finalStringBox;
    public Text MMSIBox;
    public Text SessionBox;
    public Text Limit;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void GenerateString()
    {
        if (MMSIBox.text == "")
        {
            Debug.Log("MMSI is blank");            
        }

        else
        {
            Debug.Log("MMSI: " + MMSIBox.text);
            mmsi = MMSIBox.text;
            mmsi = "&mmsi=" + mmsi;
            //&session=314&limit=20&mmsi=4575755
        }

        if (SessionBox.text == "")
        {
            Debug.Log("Session is blank");
        }

        else
        {
        Debug.Log("Session: " + SessionBox.text);
        session = SessionBox.text;
        session = "&session=" + session;
        }

        if (Limit.text =="")
        {
            Debug.Log("Limit is blank");
        }

        else
        {
            Debug.Log("Limit: " + Limit.text);
            limit = Limit.text;
            limit = "&limit=" + limit;
        }
        
        finalAISString = baseAISString + "*********" + session + limit + mmsi;
        Debug.Log("finalAISString: " + finalAISString);
        finalStringBox.text = finalAISString;
    }
    
}
