﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;
using System.Net;
using System.Linq;
using UnityEngine.VR;
using System.Collections;

public class SpawnBoats : MonoBehaviour
{
    public List<GameObject> ships = new List<GameObject>();
    public List<dynamicAIS> dynamicShipsList = new List<dynamicAIS>();
    public List<dynamicAIS> shipStartMoves = new List<dynamicAIS>();
    public List<dynamicAIS> shipMoves = new List<dynamicAIS>();
    public List<dynamicAIS> deletedShipMoves = new List<dynamicAIS>();
    public List<staticAIS> StaticShipNamelist = new List<staticAIS>();

    List<TextController> textObjects = new List<TextController>();

    public GameObject currentShip;
    public GameObject updateShip;
    public GameObject obj;
    public GameObject camObj;

    public GameObject textObj;
    public Vector3 pos;
    public Quaternion rotation;
    public Vector3 offset;

    private TextController tc;

    private GameObject newShip;
    private GameObject newTextObj;
    public string line;

    public staticAIS sAIS;
    public dynamicAIS dAIS;

    public bool first = true;




    public Rigidbody rb;
    void Start()
    {

    //GvrViewer.Instance.VRModeEnabled = true﻿; old command to enable VR on the fly

        StartCoroutine(LoadDevice("cardboard"));
                
        try
        {
            Debug.Log("Trying to download ships!");
            downloadDynamicShips();
        }
        catch (Exception e)
        {
            Debug.Log("The data could not be read:\n" + e.Message);
        }

        CountTime.countTime = 0;
        Time.timeScale = 1;

    }

    //code to change scene to VR on the fly
    IEnumerator LoadDevice(string newDevice)
    {
        VRSettings.LoadDeviceByName(newDevice);
        yield return null;
        VRSettings.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 0) //If game is paused don't don't do anything
        {
            return;
        }

        if (ShipData.reverseTime == true) //If game is reverse, change to reverse Method
        {
            reverseTime();
            return;
        }

        foreach (GameObject sh in ships)
            for (int i = 0; i < ships.Count; i++)
            {
            //move ship by the positions its facing every frame
            sh.transform.position += (sh.transform.forward * 1 / 10 * Time.deltaTime);
            
            //ships[i].transform.position += (ships[i].transform.forward * 1 / 10 * Time.deltaTime);
            //ships[i].GetComponent<Rigidbody>().velocity = transform.forward;
            //sh.transform.Translate(transform.forward * 1);
            //sh.transform.Translate(0f,70,0f, Space.World); changes position
            }

        /*foreach (dynamicAIS i in shipMoves.ToList())
        {
            Debug.Log("SHIP MOVES: " + i);
        }*/

        //foreach (dynamicAIS i in shipMoves.ToList())
        if (shipMoves.Count > 0)
        {
            for (int i = 0; i < shipMoves.Count; i++)
            {

                //Debug.Log("convertTime(i.Time_stamp_system): " + convertTime(i.Time_stamp_system));
                //Debug.Log("GlobalVariables.StartCountTime: " + GlobalVariables.StartCountTime);
                //Debug.Log("CountTime.countTime: " + CountTime.countTime);
                GlobalVariables.StartCountTime = 50642;
                //GlobalVariables.StartCountTime = 50606; actual start
                double triggerTime = (convertTime(shipMoves[i].Time_stamp_system) - GlobalVariables.StartCountTime);
                //if the timer is in front of the AIS message time than it triggers an update
                if (CountTime.countTime >= triggerTime)
                {
                    Debug.Log("Compare Calc: " + triggerTime + " >= " + CountTime.countTime);
                    Debug.Log("Update Position and Delete Triggered!");


                    updateShip = GameObject.Find(shipMoves[i].MMSI.ToString());
                    Vector3 r = new Vector3(0.0f, shipMoves[i].True_heading, 0.0f);
                    updateShip.transform.eulerAngles = r;
                    //Vector3 velocity = new Vector3(0.0f, 0.0f, shipMoves[i].SOG.ToFloat());
                    //Rigidbody rb = updateShip.GetComponent<Rigidbody>();
                    //rb.AddForce(velocity);

                    Debug.Log("Ship removed: " + shipMoves[i]);
                    Debug.Log("ShipToMoveCount: " + (shipMoves.Count - 1));
                    deletedShipMoves.Add(shipMoves[i]);
                    shipMoves.Remove(shipMoves[i]);

                }
            }
        }

    }

    private void reverseTime()//this is the switched into from update if time is in reverse.
    {
        
        if (CountTime.countTime < 0.1)//Stops the timers from going negative
        {
            foreach (GameObject sh in ships)
            {
            }
            return;
        }

        if (Time.timeScale == 0) //If game is paused don't don't do anything
        {

            return;
        }

        foreach (GameObject sh in ships)
            for (int i = 0; i < ships.Count; i++)
            {
                //move ship by the positions its facing every frame
                ships[i].transform.position += (ships[i].transform.forward * -1/10 * Time.deltaTime);
                //ships[i].transform.position += (ships[i].transform.forward * -1 / 10 * Time.deltaTime);
                //ships[i].transform.position -= ((ships[i].transform.forward * 1 )/400);

                //sh.transform.Translate(transform.forward * 1);
                //sh.transform.Translate(0f,70,0f, Space.World); changes position
                // sh.transform.position += -transform.forward * Time.deltaTime;
                //ti.Insert(0, initialItem);
            }

        if (deletedShipMoves.Count > 0)
        {
            for (int i = 0; i < deletedShipMoves.Count; i++)
            {

                double reverseTriggerTime = (convertTime(deletedShipMoves[i].Time_stamp_system) - GlobalVariables.StartCountTime);
                if (CountTime.countTime <= reverseTriggerTime)
                {
                    //Debug.Log("Compare Calc: " + reverseTriggerTime + " >= " + CountTime.countTime);
                    Debug.Log("Reverse Update Position and Undelete Triggered!");


                    updateShip = GameObject.Find(deletedShipMoves[i].MMSI.ToString());
                    //Vector3 r = new Vector3(0.0f, deletedShipMoves[i].True_heading, 0.0f);
                    Vector3 r = new Vector3(0.0f, shipStartMoves[i].True_heading, 0.0f);
                    updateShip.transform.eulerAngles = r;
                    Vector3 velocity = new Vector3(0.0f, 0.0f, deletedShipMoves[i].SOG.ToFloat());
                    //Rigidbody rb = updateShip.GetComponent<Rigidbody>();
                    //rb.AddForce(velocity);
                    Debug.Log("Find Ship: " + deletedShipMoves[i].MMSI.ToString());
                    Debug.Log("Angle of ship: " + deletedShipMoves[i].True_heading);

                    
                    shipMoves.Insert(0, deletedShipMoves[i]);
                    Debug.Log("Ship removed from deleted Moves: " + deletedShipMoves[i]);
                    deletedShipMoves.Remove(deletedShipMoves[i]);                    
                    Debug.Log("DeletedShipMovesCount: " + (deletedShipMoves.Count ));
                }
            }
        }

        if (CountTime.countTime <= .01)
        {
            for (int i = 0; i < shipMoves.Count; i++)
            {
                Debug.Log(shipMoves[i]);
            }
        }

    }
    
    private void instantiateNewDynamicShip(dynamicAIS i)
    {
        double Easting;
        double Northing;
        double lon = i.Longitude;
        double lat = i.Latitude;
        
        convertToGPS(lat, lon, out Easting, out Northing);

        lat = Easting;
        lon = Northing;
        //Debug.Log("LONG: " + lon + " LAT: " + lat);
        //Put coordinates in map boundary
        lat = (lat - 530000) / 100;
        lon = (lon - 6970000) / 100;
        //Debug.Log("LONG: " + lon + " LAT: " + lat);

        //lat is easting, long is northing
        pos = new Vector3((float)lat, (float)0, (float)lon);
       
        //pos = new Vector3(lon, 1 , lat);
        newShip = (GameObject)Instantiate(obj, pos, rotation);

        newShip.name = i.MMSI.ToString();
        newShip.transform.localScale += new Vector3(1, 1, 1);

        rb = newShip.GetComponent<Rigidbody>();
        //rb = transform.forward * 20;
        Vector3 r = new Vector3(0.0f, i.True_heading, 0.0f);
        //Vector3 r = new Vector3(0.0f, 90, 0.0f);
        //Vector3 velocity = new Vector3(0.0f, 0.0f, i.SOG.ToFloat());
        newShip.transform.eulerAngles = r;
        //rb.AddForce(velocity);
        
        //rb.rotation(0, i.ROT, 0);
        
        //rb.velocity = transform.forward * i.SOG.ToFloat();
        rb.velocity = r * (i.SOG.ToFloat());

        //rb.transform.Translate(transform.TransformDirection(r));
        //rb.transform.TransformDirection(r);
        //
        //rb.AddForce(transform.forward * i.SOG.ToFloat() * Time.deltaTime);
        //rb.velocity = transform.InverseTransformDirection(Vector3.forward) * i.SOG.ToFloat();

        //newShip.transform.position += -transform.forward * Time.deltaTime;

        //Instantiate text and get a red to it in newTextObj
        //newTextObj = (GameObject)Instantiate(textObj, pos + offset, transform.rotation);
        newTextObj = (GameObject)Instantiate(textObj, pos + new Vector3(0,5,0), transform.rotation);

        //sets textObj as child of generated ship object
        newTextObj.transform.parent = newShip.transform;

        //tc = newTextObj.GetComponent<TextController>();
        //tc.target = newShip;
        //tc.offset = offset;
    
        //change text code here
        newTextObj.GetComponent<TextMesh>().fontSize = 80;
        newTextObj.GetComponent<TextMesh>().text = i.MMSI.ToString();

        //textObjects.Add(tc);
        ships.Add(newShip);

    }

    public void downloadDynamicShips()
    {       
        try
        {
            WebClient wc = new System.Net.WebClient();
            //List<dynamicAIS> list = new List<dynamicAIS>();
            //var data = wc.DownloadString("http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/ais_voyages_dynamic.php?passwd=m3F4gaWX9M&session=374&limit=20");
            var data = wc.DownloadString("http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/ais_voyages_dynamic.php?passwd=m3F4gaWX9M&session=314&limit=20");
            //byte[] raw = wc.DownloadData("http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/ais_voyages_dynamic.php?passwd=m3F4gaWX9M&session=314&limit=1");
            //byte[] raw = wc.DownloadData("http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/ais_voyages_dynamic.php?passwd=m3F4gaWX9M&session=314");
            //var data = wc.DownloadString("http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/ais_voyages_dynamic.php?passwd=m3F4gaWX9M&limit=10&mmsi=4575755");

            Console.WriteLine("DATA RECIEVED");

            var items = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamicAIS>>(data);
            
            foreach (var i in items)
            {
                Debug.Log(i);
                if (dynamicShipsList.Count ==0)
                {
                    GlobalVariables.StartCountTime = convertTime(i.Time_stamp_system);
                    Debug.Log("GlobalVariables.StartCountTime " + GlobalVariables.StartCountTime);
                    first = false;
                }

                if (!dynamicShipsList.Any(p => p.MMSI == i.MMSI))
                {
                    dynamicShipsList.Add(i);
                    shipStartMoves.Add(i);
                    instantiateNewDynamicShip(i);
                }
                else
                {
                    shipMoves.Add(i);
                }

            }

            /*foreach (dynamicAIS item in dynamicShipsList)
            {
                                Debug.Log(item);
            }*/

            GlobalVariables.GlobaldynamicShipsList = dynamicShipsList;

            //try to force garbage collection
            wc.Dispose();
            items = null;
            data = null;
            wc = null;

        }
        catch (WebException e)
        {
            Debug.Log("Invalid input sent to server");
            throw e;            
        }
    }

    public void downloadStaticShips()
    {
        /*wc = new System.Net.WebClient();
    List<staticAIS> list3 = new List<staticAIS>();
    var data2 = wc.DownloadString("http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/ais_voyages_static.php?passwd=m3F4gaWX9M&session=314&limit=2");
    var list2 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<staticAIS>>(data2);

    /*foreach (var i in list2)
    {
    //Console.WriteLine(i);
    list3.Add(i);
    }

    foreach (staticAIS i in list3)
    {
    Console.WriteLine(i);
    }*/

    }

    //converts the AIS string time to a format usable by our timer class
    public double convertTime(string i)
    {
        string time = i.Substring(i.Length - 8, 8);
        double seconds = TimeSpan.Parse(time).TotalSeconds;
        //Debug.Log("SECONDS: " + seconds);
        return seconds;
    }

    //Debug Method used for displaying the ships in the dynamicShipsList to console.
    public void readDynamicShips()
    {
        foreach (dynamicAIS i in dynamicShipsList)
        {
            instantiateNewDynamicShip(i);
        }

        Debug.Log("Dynamic Boat Count: " + dynamicShipsList.Count);
        
    }

    //Converts a longitude and latitude to UTM Northing and Easting
    public static void convertToGPS(double Lat, double Lon, out double Easting, out double Northing)
    {
        int Zone = (int)Math.Floor(Lon / 6 + 31);
        Char Letter = 'J'; 
        Console.WriteLine("Zone" + Zone);
        if (Lat < -72)
            Letter = 'C';
        else if (Lat < -64)
            Letter = 'D';
        else if (Lat < -56)
            Letter = 'E';
        else if (Lat < -48)
            Letter = 'F';
        else if (Lat < -40)
            Letter = 'G';
        else if (Lat < -32)
            Letter = 'H';
        else if (Lat < -24)
            Letter = 'J';
        else if (Lat < -16)
            Letter = 'K';
        else if (Lat < -8)
            Letter = 'L';
        else if (Lat < 0)
            Letter = 'M';
        else if (Lat < 8)
            Letter = 'N';
        else if (Lat < 16)
            Letter = 'P';
        else if (Lat < 24)
            Letter = 'Q';
        else if (Lat < 32)
            Letter = 'R';
        else if (Lat < 40)
            Letter = 'S';
        else if (Lat < 48)
            Letter = 'T';
        else if (Lat < 56)
            Letter = 'U';
        else if (Lat < 64)
            Letter = 'V';
        else if (Lat < 72)
            Letter = 'W';
        else
            Letter = 'X';
        Easting = 0.5 * Math.Log((1 + Math.Cos(Lat * Math.PI / 180) * Math.Sin(Lon * Math.PI / 180 - (6 * Zone - 183) * Math.PI / 180)) / (1 - Math.Cos(Lat * Math.PI / 180) * Math.Sin(Lon * Math.PI / 180 - (6 * Zone - 183) * Math.PI / 180))) * 0.9996 * 6399593.62 / Math.Pow((1 + Math.Pow(0.0820944379, 2) * Math.Pow(Math.Cos(Lat * Math.PI / 180), 2)), 0.5) * (1 + Math.Pow(0.0820944379, 2) / 2 * Math.Pow((0.5 * Math.Log((1 + Math.Cos(Lat * Math.PI / 180) * Math.Sin(Lon * Math.PI / 180 - (6 * Zone - 183) * Math.PI / 180)) / (1 - Math.Cos(Lat * Math.PI / 180) * Math.Sin(Lon * Math.PI / 180 - (6 * Zone - 183) * Math.PI / 180)))), 2) * Math.Pow(Math.Cos(Lat * Math.PI / 180), 2) / 3) + 500000;
        Easting = Math.Round(Easting * 100) * 0.01;


        Console.WriteLine("Easting " + Easting);
        Northing = (Math.Atan(Math.Tan(Lat * Math.PI / 180) / Math.Cos((Lon * Math.PI / 180 - (6 * Zone - 183) * Math.PI / 180))) - Lat * Math.PI / 180) * 0.9996 * 6399593.625 / Math.Sqrt(1 + 0.006739496742 * Math.Pow(Math.Cos(Lat * Math.PI / 180), 2)) * (1 + 0.006739496742 / 2 * Math.Pow(0.5 * Math.Log((1 + Math.Cos(Lat * Math.PI / 180) * Math.Sin((Lon * Math.PI / 180 - (6 * Zone - 183) * Math.PI / 180))) / (1 - Math.Cos(Lat * Math.PI / 180) * Math.Sin((Lon * Math.PI / 180 - (6 * Zone - 183) * Math.PI / 180)))), 2) * Math.Pow(Math.Cos(Lat * Math.PI / 180), 2)) + 0.9996 * 6399593.625 * (Lat * Math.PI / 180 - 0.005054622556 * (Lat * Math.PI / 180 + Math.Sin(2 * Lat * Math.PI / 180) / 2) + 4.258201531e-05 * (3 * (Lat * Math.PI / 180 + Math.Sin(2 * Lat * Math.PI / 180) / 2) + Math.Sin(2 * Lat * Math.PI / 180) * Math.Pow(Math.Cos(Lat * Math.PI / 180), 2)) / 4 - 1.674057895e-07 * (5 * (3 * (Lat * Math.PI / 180 + Math.Sin(2 * Lat * Math.PI / 180) / 2) + Math.Sin(2 * Lat * Math.PI / 180) * Math.Pow(Math.Cos(Lat * Math.PI / 180), 2)) / 4 + Math.Sin(2 * Lat * Math.PI / 180) * Math.Pow(Math.Cos(Lat * Math.PI / 180), 2) * Math.Pow(Math.Cos(Lat * Math.PI / 180), 2)) / 3);
        if (Letter < 'M')
            Northing = Northing + 10000000;
            

        Northing = Math.Round(Northing * 100) * 0.01;
        Console.WriteLine("Northing " + Northing);

    }
    
    //Can be used to convert Northing and Easting UTM back to lon\lat coordinates
    public static void ToLatLon(double utmX, double utmY, string utmZone, out double latitude, out double longitude)
    {
        bool isNorthHemisphere = utmZone.Last() >= 'N';

        var diflat = -0.00066286966871111111111111111111111111;
        var diflon = -0.0003868060578;

        var zone = int.Parse(utmZone.Remove(utmZone.Length - 1));
        var c_sa = 6378137.000000;
        var c_sb = 6356752.314245;
        var e2 = Math.Pow((Math.Pow(c_sa, 2) - Math.Pow(c_sb, 2)), 0.5) / c_sb;
        var e2cuadrada = Math.Pow(e2, 2);
        var c = Math.Pow(c_sa, 2) / c_sb;
        var x = utmX - 500000;
        var y = isNorthHemisphere ? utmY : utmY - 10000000;

        var s = ((zone * 6.0) - 183.0);
        var lat = y / (c_sa * 0.9996);
        var v = (c / Math.Pow(1 + (e2cuadrada * Math.Pow(Math.Cos(lat), 2)), 0.5)) * 0.9996;
        var a = x / v;
        var a1 = Math.Sin(2 * lat);
        var a2 = a1 * Math.Pow((Math.Cos(lat)), 2);
        var j2 = lat + (a1 / 2.0);
        var j4 = ((3 * j2) + a2) / 4.0;
        var j6 = ((5 * j4) + Math.Pow(a2 * (Math.Cos(lat)), 2)) / 3.0;
        var alfa = (3.0 / 4.0) * e2cuadrada;
        var beta = (5.0 / 3.0) * Math.Pow(alfa, 2);
        var gama = (35.0 / 27.0) * Math.Pow(alfa, 3);
        var bm = 0.9996 * c * (lat - alfa * j2 + beta * j4 - gama * j6);
        var b = (y - bm) / v;
        var epsi = ((e2cuadrada * Math.Pow(a, 2)) / 2.0) * Math.Pow((Math.Cos(lat)), 2);
        var eps = a * (1 - (epsi / 3.0));
        var nab = (b * (1 - epsi)) + lat;
        var senoheps = (Math.Exp(eps) - Math.Exp(-eps)) / 2.0;
        var delt = Math.Atan(senoheps / (Math.Cos(nab)));
        var tao = Math.Atan(Math.Cos(delt) * Math.Tan(nab));

        longitude = ((delt * (180.0 / Math.PI)) + s) + diflon;
        latitude = ((lat + (1 + e2cuadrada * Math.Pow(Math.Cos(lat), 2) - (3.0 / 2.0) * e2cuadrada * Math.Sin(lat) * Math.Cos(lat) * (tao - lat)) * (tao - lat)) * (180.0 / Math.PI)) + diflat;
    }
}



public static class MathExtensions
{
    public static float ToFloat(this double value)
    {
        return (float)value;
    }
}